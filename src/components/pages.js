import React from 'react'
import Favorites from '../microComponents/favorites'
import Home from '../microComponents/home'
import Profile from '../microComponents/profile'
import Search from '../microComponents/search'
import ShoppingCart from '../microComponents/shoppingCart'

export default function Pages(props) {
    const {active} = props

    return (
        <>
        {active===1 && <Home  />}
        {active===2 && <Search />}
        {active===3 && <ShoppingCart />}
        {active===4 && <Favorites />}
        {active===5 && <Profile />}
        </>
    )
}
