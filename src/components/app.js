import React, { useRef, useState } from "react";
import Icons from "./icons";
import Pages from "./pages";
import Top from "./top";
import "../assets/index.css";
import "../assets/pagesCss/pagesCss.css";
import { useEffect } from "react/cjs/react.development";

export default function App() {
  const ref = useRef();

  const [active, setActive] = useState(1);
  useEffect(() => {
    const changePage = ref.current.firstChild
    // changePage.style.background = "purple"
    changePage.style.height= "100%"
    changePage.style.top = "0"
  
  });
  const setA = (props) => {
    setActive(props);
  };
  return (
    <div id="parent">
      <div id="top" >
        <Top />
      </div>
      <div id="pages" ref={ref}>
        <Pages active={active} />
      </div>
      <div id="icons">
        <Icons active={active} setA={setA} />
      </div>
    </div>
  );
}
