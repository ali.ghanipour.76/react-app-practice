import React, { useState } from "react";
import "../assets/iconsCss/iconsCss.css";

const bottomNavigation = [
  {
    name: "خانه",
    icon: "https://img.icons8.com/ios-filled/30/000000/home.png",
    id: 1,
  },
  {
    name: "جستجو",
    icon: "https://img.icons8.com/ios/30/000000/search--v1.png",
    id: 2,
  },
  {
    name: "سبد خرید",
    icon: "https://img.icons8.com/external-kiranshastry-lineal-kiranshastry/30/000000/external-shopping-cart-interface-kiranshastry-lineal-kiranshastry.png",
    id: 3,
  },
  {
    name: "علاقمندی ها",
    icon: "https://img.icons8.com/ios/30/000000/hearts--v1.png",
    id: 4,
  },
  {
    name: "پروفایل",
    icon: "https://img.icons8.com/dotty/30/000000/test-account.png",
    id: 5,
  },
];
export default function Icons(props) {
  // const [active, setActive] = useState(1);
  const { active, setA } = props;
  const tabItemMap = (item, index) => {
    console.log(item, index);
    return <TabItem name={item.name} icon={item.icon} id={item.id} active={active} setA={setA} />
  };
  return (
    <>
      {bottomNavigation.map(tabItemMap)}
    </>
  );
}
function TabItem(props) {
  console.log(props);
  const { active, setA , name , id , icon} = props;
  const Activate = (prop) => {
    setA(prop);
  };
  return (
    <div
      className= {`icons ${id === 1 ? "home" : ""} ${id===5 ? "profile" : ""} ${active===id ? "active1" : ""}`}
      onClick={() => Activate(id)}
    >
      <img src={icon} />
      <span>{name}</span>
    </div>
  );
}
